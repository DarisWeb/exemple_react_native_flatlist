import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'

export default UserRow = props => (
  <View style={styles.row}>
    <View>
      <View  style={styles.row2}>
        <Text style={styles.primaryText}>{props.name}</Text>
        <Text style={styles.primaryText}>{props.sum + '€'}</Text>
      </View>
      <Text style={styles.secondaryText}>{props.date}</Text>
    </View>
  </View>
)

const styles = StyleSheet.create({
  row: { flexDirection: 'row', alignItems: 'center', padding: 12 },
  row2: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'},
  picture: { width: 50, height: 50, borderRadius: 25, marginRight: 18 },
  primaryText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'black',
    marginBottom: 4,
  },
  secondaryText: { color: 'grey' },
})
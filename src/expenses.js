export let expenses = [
    {
        "id": 21,
        "name": "Animi dolor.",
        "sum": 330811239.07,
        "date": "2021-03-31",
        "category": {
            "id": 42,
            "name": "Voluptatem.",
            "description": "Veniam quod quia blanditiis eveniet dolorum fugit nam et laborum possimus repellendus maiores eveniet numquam impedit ad voluptates ab atque et."
        },
        "user": []
    },
    {
        "id": 22,
        "name": "Consequatur ex.",
        "sum": 82250450.65,
        "date": "2021-02-14",
        "category": null,
        "user": null
    },
    {
        "id": 23,
        "name": "Modi iusto.",
        "sum": 136.03,
        "date": "2021-04-15",
        "category": null,
        "user": null
    },
    {
        "id": 24,
        "name": "Velit modi.",
        "sum": 35.1,
        "date": "2021-03-11",
        "category": null,
        "user": null
    },
    {
        "id": 25,
        "name": "Itaque.",
        "sum": 0.35,
        "date": "2021-04-21",
        "category": null,
        "user": null
    },
    {
        "id": 26,
        "name": "Adipisci.",
        "sum": 40867,
        "date": "2021-04-20",
        "category": null,
        "user": null
    },
    {
        "id": 27,
        "name": "Mollitia deleniti.",
        "sum": 5.42,
        "date": "2021-03-12",
        "category": null,
        "user": null
    },
    {
        "id": 28,
        "name": "Explicabo laudantium.",
        "sum": 36.83,
        "date": "2021-03-28",
        "category": null,
        "user": null
    }
]
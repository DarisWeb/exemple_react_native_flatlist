import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import axios from 'axios';
import UserList from './src/components/UserList'
//import { expenses } from './src/expenses'

function getdata() {
  // Création d'une promesse qui va contenir l'appel
  const promise = axios.get(`http://127.0.0.1:8000/api/expenses`);

  // Avec la méthode "then", on extrait les données
  const data = promise.then((reponse) => console.log(reponse.data));

  //  On retourne les données
  return data;
}

function componentDidMount(){
  axios.get(`http://127.0.0.1:8000/api/expenses`)
    .then(res => {
      const data = res.data;
      this.setState({ data });
    })
  
}

export default class App extends Component {
  state = {
    data: []
  }

  

  render() {
    return (
      <View style={styles.container}>
        <UserList data={ getdata() } />
      </View>
    )
  }
}

const styles = StyleSheet.create({ container: { flex: 1, paddingTop: 20 } })